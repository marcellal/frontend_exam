import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  constructor(private uList:ApiService) { }

  ngOnInit() {
  }

  
  newName : string = "";
  newAddress : string = "";
  newEmail : string = "";
  newPhone : string = "";
  newCompany : string = "";

  addUser(){
    // var id = 1;
    // if (this.uList.userList.length > 0){
    //   id = this.uList.userList[this.uList.userList.length - 1]["id"] + 1;
    // }
    this.uList.userList.push({"name":this.newName, "address":this.newAddress, "email":this.newEmail, "phone":this.newPhone,"company": this.newCompany});

    this.newName = "";
    this.newEmail = "";
    this.newAddress = "";
    this.newPhone = "";
    this.newCompany = "";
    
  }
}

import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor(private uList:ApiService) { }

  ngOnInit() {
  }

  remove(index){
    this.uList.userList.splice(index,1);
  }
  

}
